export const ApiConfig = Object.freeze({
  baseUrl: "http://example.com/api",
  contentType: "application/json; charset=utf-8"
});
