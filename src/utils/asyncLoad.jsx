import {Skeleton} from "antd";
import progress from "nprogress";
import "nprogress/nprogress.css";
import React, {Component} from "react";

progress.configure({showSpinner: false});

const asyncLoad = (myComponent) => {
  class AsyncComponent extends Component {
    constructor(props) {
      super(props);
      this.state = {
        component: null
      };
      this.mounted = false;
      progress.start();
    }

    componentWillUnmount() {
      this.mounted = false;
    }

    componentDidMount = async () => {
      this.mounted = true;
      const { default : MyComponent } = await myComponent();
      progress.done();
      if (this.mounted) {
        this.setState({
          component: MyComponent
        });
      }
    };

    render() {
      const MyComponent = this.state.component;
      return <Skeleton active={true} loading={MyComponent === null} paragraph={true}>
        {MyComponent ? <MyComponent {...this.props} /> : <div/>}
      </Skeleton>;
    }
  }
  return AsyncComponent;
};

export default asyncLoad;