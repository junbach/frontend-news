export {default as asyncLoad} from "./asyncLoad";
export {default as restClient} from "./restClient";
/**
 * Build url string by input params
 * @param inputs {string} to join and build to url
 */
export const buildLink = (...inputs) =>
  inputs ? `/${inputs.join("/")}`
    .replace(/\/+/, "/")
    .toLowerCase() : "/";
