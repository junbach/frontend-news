import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import {ApiConfig} from "./properties";
import fakeNews from "./fakeNews";
import lodash from "lodash";
import moment from "moment";

const requestConfig = {
  baseUrl: ApiConfig.baseUrl,
  headers: {
    "Content-Type": ApiConfig.contentType
  }
};
const restClient = axios.create(requestConfig);

// Fake response data
const newsList = lodash.range(38).map((_, i) => ({
  title: `${i % 2 === 0 ? fakeNews.title_0 : fakeNews.title_1} n_${i+1}`,
  preview: fakeNews.preview,
  updatedAt: moment().format("DD MMM, YYYY HH:mm")
}));
const mock = new MockAdapter(restClient, {delayResponse: 200});
mock.onGet("/news").reply(config => {
  const {params: {page, search, pageSize}} = config;
  if (isNaN(page) || page < 1) {
    return [400, {error: "invalid page number", code: 400}];
  }
  const total = newsList.filter(item => lodash.isEmpty(search) || item.title.includes(search));
  const rs = total.filter((_, i) => i >= (page-1) * pageSize && i < page * pageSize);
  return [200, { total: total.length, data: rs, page, pageSize, search }];
});
// End fake news data

export default restClient;
