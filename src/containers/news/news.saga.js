import {notification} from "antd";
import {all, call, fork, put, takeEvery} from "redux-saga/effects";
import {appendNewsFn, NewsActionType} from "./news.reducer";
import {restClient} from "../../utils";

const loadNewsWatcher = function* () {
  yield takeEvery(NewsActionType.LOAD_NEWS, function* ({payload: {page, search, pageSize}}) {
    try {
      const rs = yield call(getNews, page, search, pageSize);
      yield put(appendNewsFn(rs));
    } catch (e) {
      yield notification.error({message: "Loading news error"});
    }
  });
};

const getNews = async (page, search, pageSize) =>
  await restClient.get("/news", {params: {page, search, pageSize: pageSize || 15}})
    .then(resp => resp.data);

export default function* saga() {
  yield all([
    fork(loadNewsWatcher)
  ]);
}
