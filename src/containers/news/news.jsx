import "./news.css";
import {Button, List} from "antd";
import PropTypes from "prop-types";
import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {loadNewsFn} from "./news.reducer";
import {NewsBox} from "../../components/news";

class NewsContainer extends Component {
  constructor(props) {
    super(props);
    if (this.props.data.length === 0) {
      this.props.loadNews({page: 1, pageSize: props.pageSize});
    }
  }

  loadMore = () => {
    const {loadNews, page, pageSize, search} = this.props;
    loadNews({pageSize, search, page: page+1});
  };

  render() {
    const {loading, data, pageSize} = this.props;
    const loadMoreBtn = data.length%pageSize === 0 ? <Button block onClick={this.loadMore}>Load More</Button> : "";
    const gridLayout = {gutter: 40, md: 3, sm: 1};
    return <div>
      <List
        grid={gridLayout}
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMoreBtn}
        dataSource={data}
        renderItem={item => <NewsBox {...item}/>}
      />
    </div>;
  }
}

NewsContainer.propTypes = {
  data: PropTypes.array,
  page: PropTypes.number,
  pageSize: PropTypes.number,
  loading: PropTypes.bool,
  search: PropTypes.string,
  loadNews: PropTypes.func
};

const mapStateToProps = (state) => ({...state.NewsReducer});
const mapDispatchToProps = (dispatch) => ({
  loadNews: bindActionCreators(loadNewsFn, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(NewsContainer);
