export const NewsActionType = {
  LOAD_NEWS: "LOAD_NEWS",
  APPEND_NEWS: "APPEND_NEWS"
};

export const appendNewsFn = (payload) => ({
  type: NewsActionType.APPEND_NEWS,
  payload
});

export const loadNewsFn = (payload) => ({
  type: NewsActionType.LOAD_NEWS,
  payload
});

const initState = {
  page: 1,
  pageSize: 15,
  loading: false,
  data: [],
  search: undefined,
};

const reducer = (state = initState, {type, payload}) => {
  switch (type) {
  case NewsActionType.LOAD_NEWS:
    return {...state, loading: true};
  case NewsActionType.APPEND_NEWS:
    // eslint-disable-next-line
    const data = payload.search != state.search ? payload.data : [...state.data, ...payload.data];
    return {
      loading: false,
      page: payload.page,
      pageSize: payload.pageSize,
      search: payload.search,
      data
    };
  default:
    return state;
  }
};

export default reducer;
