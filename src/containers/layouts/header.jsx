import "./header.css";
import React, {Component} from "react";
import {Col, Icon, Input, Row} from "antd";
import {bindActionCreators} from "redux";
import {loadNewsFn} from "../news/news.reducer";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

class HeaderContainer extends Component {
  searchNews = (e) => {
    const {loadNews} = this.props;
    loadNews({page: 1, search: e.target.value});
  };

  render() {
    return <div className="header-container">
      <div className="logo">Logo</div>
      <div className={"collapse-btn"}>
        <Icon type="menu" />
      </div>
      <Row className="menu-nav" align="middle">
        <Link to="/news"><Col md={4} sm={24} xs={24} className="menu-col menu-active">News</Col></Link>
        <Link to="/regions"><Col md={4} sm={24} xs={24} className="menu-col">Regions</Col></Link>
        <Link to="/video"><Col md={4} sm={24} xs={24} className="menu-col">Video</Col></Link>
        <Link to="/tv"><Col md={4} sm={24} xs={24} className="menu-col">TV</Col></Link>
        <Col md={8} sm={24} xs={24} className="search-col">
          <Input
            placeholder="Search"
            prefix={<Icon type="search" style={{ color: "rgba(0,0,0,.25)" }} />}
            className="search-input"
            onPressEnter={this.searchNews}
          />
        </Col>
      </Row>
    </div>;
  }
}
HeaderContainer.propTypes = {
  loadNews: PropTypes.func
};
const mapDispatchToProps = (dispatch) => ({
  loadNews: bindActionCreators(loadNewsFn, dispatch)
});
export default connect(null, mapDispatchToProps)(HeaderContainer);
