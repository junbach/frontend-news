import {connectRouter} from "connected-react-router";
import {combineReducers} from "redux";
import {all} from "redux-saga/effects";
import {NewsReducer, NewsSaga} from "./news";

export const rootReducer =  (history) => combineReducers({
  router: connectRouter(history),
  NewsReducer
});

export const rootSaga = function*() {
  yield all([
    NewsSaga()
  ]);
};
