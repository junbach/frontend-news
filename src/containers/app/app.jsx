import React, {Component} from "react";
import {Layout} from "antd";
import "./app.css";
import {FooterContainer, HeaderContainer} from "../layouts";
import {AppRoutes} from "./app.routes";
const { Header, Footer, Content } = Layout;
export default class App extends Component {
  render() {
    return (
      <Layout>
        <Header>
          <HeaderContainer />
        </Header>
        <Content>
          <AppRoutes url="/" />
        </Content>
        <Footer>
          <FooterContainer />
        </Footer>
      </Layout>
    );
  }
}
