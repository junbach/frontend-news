import React from "react";
import PropTypes from "prop-types";
import {Redirect, Route, Switch} from "react-router";
import {buildLink} from "../../utils";
import {asyncLoad} from "../../utils";

const AppRoutes = ({url}) => (
  <Switch>
    <Route exact={true} path={url} render={() => <Redirect to={buildLink(url, "news")} />}/>
    <Route exact={true} path={buildLink(url, "news")} component={asyncLoad(() => import("../news"))} />
    <Route component={asyncLoad(() => import("../exceptions/404"))} />
  </Switch>
);

AppRoutes.propTypes = {
  url: PropTypes.string
};

export {AppRoutes} ;
