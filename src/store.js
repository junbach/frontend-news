import { routerMiddleware } from "connected-react-router";
import {createBrowserHistory} from "history";
import {applyMiddleware, createStore} from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import {rootReducer, rootSaga} from "./containers/reducers";

const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const routeMiddleware = routerMiddleware(history);
const middlewares = [sagaMiddleware, routeMiddleware];
const composeEnhancers = composeWithDevTools({});
const store = createStore(
  rootReducer(history),
  composeEnhancers(applyMiddleware(...middlewares)),
);
sagaMiddleware.run(rootSaga);
export {store, history};
