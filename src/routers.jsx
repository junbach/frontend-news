import {ConnectedRouter} from "connected-react-router";
import React from "react";
import PropTypes from "prop-types";
import {Route, Switch} from "react-router";
import {App} from "./containers/app";

const MainRoutes = ({history}) => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route path="/" component={App}/>
    </Switch>
  </ConnectedRouter>
);

MainRoutes.propTypes = {
  history: PropTypes.any.isRequired
};
export {MainRoutes};
