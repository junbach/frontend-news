import React, {memo} from "react";
import PropTypes from "prop-types";
import {Card, List} from "antd";

const {Item} = List;
const NewsBoxFn = (props) => {
  const {title, preview, updatedAt} = props;
  return <Item>
    <Card title={title} bordered>
      <p>{preview}</p>
      <p>{updatedAt}</p>
    </Card>
  </Item>;
};

NewsBoxFn.propTypes = {
  title: PropTypes.string,
  preview: PropTypes.string,
  updatedAt: PropTypes.string,
};

export default memo(NewsBoxFn);
