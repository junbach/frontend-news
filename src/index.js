import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import {MainRoutes} from "./routers";
import {history, store} from "./store";

const AppProvider = () => (
  <Provider store={store}>
    <MainRoutes history={history} />
  </Provider>
);
ReactDOM.render(<AppProvider />, document.getElementById("root"));
serviceWorker.unregister();
