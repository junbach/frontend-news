const { override, fixBabelImports, addLessLoader } = require("customize-cra");
// eslint-disable-next-line no-undef
module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      "@primary-color": "#247B82",
      "@font-family": "'Roboto Condensed', sans-serif",
    },
  }),
);
